<?php

date_default_timezone_set('Asia/Tehran');
include "bootstrap/init.php";

if(!isLoggedInUser()) {
    logout();
}

if(isset($_GET['logout'])) {
    logout();
}

if(!isLoggedIn()) {
    redirect(site_url('auth.php'));
}

$user = getLoggedInUser();
// print_r($_SESSION);

if (isset($_GET['delete_folder']) && is_numeric($_GET['delete_folder'])) {
    deleteFolder($_GET['delete_folder']);
    deleteTaskByfolderID($_GET['delete_folder']);
}
if (isset($_GET['delete_task']) && is_numeric($_GET['delete_task'])) {
    deleteTask($_GET['delete_task']);
}
$folders = getFolders();
$tasks = getTasks();
if(isset($_GET['sort'])){
    $tasks = getTasks($_GET['sort']);
}

include "template/tpl-index.php";