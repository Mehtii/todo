<?php

// if(!defined('BASE_PATH')){die('<p style="color:red">Permission Denied!</p>');}
defined('BASE_PATH') or die('<p style="color:red">Permission Denied!</p>');


/*** Folder Functions ***/
function getFolders()
{
    global $pdo;
    $current_user_id = getCurrentUserId();
    $sql = "SELECT * FROM folders WHERE user_id = $current_user_id";
    $stmt = $pdo->query($sql);
    return $stmt->fetchAll(PDO::FETCH_OBJ);
}
function deleteFolder($folder_id)
{
    global $pdo;
    $sql = "DELETE FROM folders WHERE id = $folder_id";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    // return $stmt->rowCount();
}
function addFolder($folder_name)
{
    global $pdo;
    $current_user_id = getCurrentUserId();
    $sql = "INSERT INTO folders (name,user_id) VALUES (:folder_name,:user_id)";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([':folder_name' => $folder_name, ':user_id' => $current_user_id]);
    $record = $pdo->lastInsertId();
    return $record;
}
function addNewFolderRow($lastInsertFolderId)
{
    global $pdo;
    $current_user_id = getCurrentUserId();
    $sql = "SELECT id,name FROM folders WHERE id = :lastAddId AND user_id = :userID";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['lastAddId' => $lastInsertFolderId, 'userID' => $current_user_id]);
    $record = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $record;
}
/*** ***/

#------------------------------------------------------------------------------------------

/*** Tasks Functions ***/
function getTasks($sort = null)
{
    global $pdo;
    $current_user_id = getCurrentUserId();
    $folder = $_GET['folder_id'] ?? null;
    $folderCondition = '';
    if (isset($folder) && is_numeric($folder)) {
        $folderCondition = "AND folder_id = $folder";
    }
    if ($folder == 0) {
        $folderCondition = '';
    }
    if(!is_null($sort)){
       $sort = "ORDER BY created_at $sort"; 
    }
    $sql = "SELECT * FROM tasks WHERE user_id = $current_user_id $folderCondition $sort";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_OBJ);
}
function deleteTask($delete_id)
{
    global $pdo;
    $sql = "DELETE FROM tasks WHERE id = $delete_id";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
}
function deleteTaskByfolderID($folder_id)
{
    global $pdo;
    $sql = "DELETE FROM tasks WHERE folder_id = $folder_id";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
}
function addTask($task_title, $folder_id)
{
    global $pdo;
    $current_user_id = getCurrentUserId();
    $sql = "INSERT INTO tasks (user_id,title,folder_id) VALUES (:user_id,:title,:folder_id)";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['user_id' => $current_user_id, 'title' => $task_title, 'folder_id' => $folder_id]);
    return $stmt->rowCount();
}
function doneSwitch($task_id)
{
    global $pdo;
    $current_user_id = getCurrentUserId();
    $sql = "UPDATE tasks SET is_done = 1 - is_done WHERE :userID AND id = :taskID";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['taskID' => $task_id, 'userID' => $current_user_id]);
    return $stmt->rowCount();
}
/*** ***/
