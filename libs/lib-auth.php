<?php

/*** Auth Functions ***/

function getCurrentUserId()
{
    return getLoggedInUser()->id ?? 0;
}

function validEmail($email)
{
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        return true;
    } else {
        return false;
    }
}

function getUserByUsername($username)
{
    global $pdo;
    if (validEmail($username)) {
        $sql2 = "WHERE email = ?";
    } else {
        $sql2 = "WHERE name = ?";
    }
    $sql = "SELECT * FROM users $sql2";
    $stmt = $pdo->prepare($sql);
    // $stmt->execute(['username' => $username]);
    $stmt->bindValue(1, $username);
    $stmt->execute();
    $record = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $record[0] ?? null;
}

function is_blocked($username)
{
    if($username->is_blocked == 1){
        return true;
    }
}

function logout()
{
    unset($_SESSION['login']);
}

function login($username, $password)
{
    # Check the email
    $user = getUserByUsername($username);
    if (is_null($user)) {
        return 'eErr';
    }
    if(is_blocked($user)) {
        return 'blocked';
    }
    # Check the password
    if (!password_verify($password, $user->password)) {
        return 'pErr';
    }
    # Login is successfully
    $user->image = "https://www.gravatar.com/avatar/" . md5(strtolower(trim($user->email)));
    $_SESSION['login'] = $user;
}

function isLoggedIn()
{
    return isset($_SESSION['login']) ? true : false;
}

function isLoggedInUser()
{
    $logged_in_user_id = getCurrentUserId();
    global $pdo;
    $sql = "SELECT * FROM users WHERE id = $logged_in_user_id";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_OBJ);
}

function getLoggedInUser()
{
    return $_SESSION['login'] ?? null;
}

function isEmailInDB($email)
{
    global $pdo;
    $sql = "SELECT id FROM users WHERE email LIKE :email";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['email' => $email]);
    return $stmt->rowCount();
}

function register($userData)
{
    $err = '';
    $name = clearInput($userData['name']);
    $email = clearInput($userData['email']);
    $password = clearInput($userData['password']);
    # < Validation of $userData
    if (empty($name) || empty($email) || empty($password)) {
        $err = 'empty';
        return $err;
    }
    if (strlen($name) < 5) {
        $err = 'lenName';
        return $err;
    }
    if (!preg_match("/^[a-zA-Z ]*$/", $name)) {
        $err = 'uErr';
        return $err;
    }
    if (!preg_match("/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,14}$/", $password)) {
        $err = 'pErr';
        return $err;
    }
    global $pdo;
    if (isEmailInDB($email)) {
        $err = "eErr";
        return $err;
    }
    # End validation />
    $passwordHash = password_hash($password, PASSWORD_BCRYPT);
    $sql = "INSERT INTO users (name,email,password) VALUES (:name, :email, :password)";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['name' => $name, 'email' => $email, 'password' => $passwordHash]);
    return $stmt->rowCount() ?? false;
}
