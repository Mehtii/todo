<?php

// if(!defined('BASE_PATH')){die('<p style="color:red">Permission Denied!</p>');}
defined('BASE_PATH') or die('<p style="color:red">Permission Denied!</p>');


function getCurrentUrl()
{
    return 1;
}

function redirect($url)
{
    header("Location: $url");
    die();
}

function diePage($msg)
{
    echo "<div style='color: #cb4a4a; background-color: #ffe0e0; border: 1px solid #e5a2a2; padding: 30px; margin: 70px auto; border-radius: 4px; font-family: sans-serif; width: 80%; }'>$msg</div>";
    die();
}

function message($msg, $cssClass = 'info')
{
    if ($cssClass == 'success') {
        echo "<div class='$cssClass' style='background-color: #b3f5c3; border: 1px solid #a2e5a6; padding: 20px; margin: 10px auto; border-radius: 4px; font-family: sans-serif; width: 80%;'>$msg</div>";
    }
    if ($cssClass == 'error') {
        echo "<div class='$cssClass' style='color: #cb4a4a; background-color: #ffe0e0; border: 1px solid #e5a2a2; padding: 20px; margin: 10px auto; border-radius: 4px; font-family: sans-serif; width: 80%;'>$msg</div>";
    }
}

function is_ajax_request()
{
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        return true;
    }
    return false;
}

function dd($data)
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}

function site_url($uri = '')
{
    return BASE_URL . $uri;
}

function clearInput($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
