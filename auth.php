<?php

include "bootstrap/init.php";

$home_url = site_url();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $action = $_GET['action'];
    $params = $_POST;
    if ($action == 'register') {
        $result = register($params);
        if ($result == 'empty') {
            message('لطفا فیلدهای مورد نظر را تکمیل نمایید', 'error');
        } elseif ($result == 'lenName') {
            message('نام کاربری باید شامل حداقل 5 کاراکتر باشد', 'error');
        } elseif ($result == 'uErr') {
            message('نام کاربری باید فقط شامل حروف انگلیسی باشد', 'error');
        } elseif ($result == 'eErr') {
            message('You have already registered with this email!', 'error');
        } elseif ($result == 'pErr') {
            message('رمز عبور باید شامل اعداد و حروف بزرگ و کوچک باشد', 'error');
        } elseif (!$result) {
            message('Error: There is an error in Registration.', 'error');
        } else {
            message("Registration is successfull. Welcome to Todo.<br>
            <a style='color:blue; text-decoration:underline' href='{$home_url}auth.php'>Please Login</a>
            ", 'success');
        }
    } else if ($action == 'login') {
        $result = login($_POST['username'], $_POST['password']);
        if (!empty($_POST['username']) && !empty($_POST['password'])) {
            if ($result == 'eErr') {
                message('کاربری با این ایمیل یا نام کاربری وجود ندارد', 'error');
            } elseif ($result == 'blocked') {
                message('اکانت شما مسدود شده است.', 'error');
            } elseif ($result == 'pErr') {
                message('رمز عبور اشتباه است', 'error');
            } else {
                redirect(site_url());
            }
        } else {
            message('لطفا فیلدهای مربوطه را تکمیل نمایید', 'error');
        }
    } else {
        echo 'شیطونی نکن :)';
    }
}
//
include BASE_PATH . "template/tpl-auth.php";
