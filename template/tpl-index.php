<?php defined('BASE_PATH') or die('<p style="color:red">Permission Denied!</p>'); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?= BASE_URL ?>assets/css/styles.css<?= '?v=' . rand(9999, 9999999) ?>" />
    <title><?= SITE_TITLE ?></title>
</head>

<body>

    <div class="page">
        <div class="pageHeader">
            <div class="title">Dashboard</div>
            <div class="userPanel">
                <a class="imgLink" href="<?= site_url('?logout') ?>" title="log out"><i class="fa fa-sign-out"></i></a>
                <span class="username"><?= ucfirst($user->name) ?? 'Unknown user' ?></span>
                <img src="<?= $user->image ?>" width="40" height="40" />
            </div>
        </div>

        <!-- F o l d e r s-->

        <div class="main">
            <div class="nav">
                <div class="menu">
                    <div class="title">Folders</div>
                    <ul class="folder-list">
                        <a class="<?= $_GET['folder_id'] == 0 || !$_GET['folder_id'] ? 'active' : '' ?>" href="<?= site_url('?folder_id=0') ?>">
                            <li><i class="<?= $_GET['folder_id'] == 0 || !$_GET['folder_id'] ? 'fa fa-folder-open' : 'fa fa-folder' ?>"></i>All</li>
                        </a>
                        <?php foreach ($folders as $folder) : ?>
                            <div class="folders">
                                <a class="<?= $_GET['folder_id'] == $folder->id ? 'active' : '' ?>" href="<?= site_url("?folder_id=$folder->id") ?>">
                                    <li style=""><i class="<?= $_GET['folder_id'] == $folder->id ? 'fa fa-folder-open' : 'fa fa-folder' ?>"></i><?= $folder->name ?></li>
                                </a>
                                <a href="<?= site_url("?delete_folder=$folder->id") ?>" class="remove">X</a>
                            </div>
                        <?php endforeach; ?>
                    </ul>

                    <div>
                        <input type="text" id="newFolderInput" placeholder="Add New Folder" />
                        <button id="newFolderBtn" class="btn">+</button>
                    </div>
                </div>
            </div>

            <!-- E n d - F o l d e r s-->

            <div class="view">
                <div class="viewHeader">
                    <div class="title">
                        <input type="text" id="newTaskInput" placeholder="Add New Task" />
                    </div>
                    <div class="functions">
                        <div class="button active"><a class="sort" href="?sort=desc&folder_id=<?= $_GET['folder_id'] ?? 0 ?>">Sort Tasks (DESC) <i class="fa fa-sort-amount-desc"></i></a></div>
                        <div class="button active"><a class="sort" href="?sort=asc&folder_id=<?= $_GET['folder_id'] ?? 0 ?>">Sort Tasks (ASC) <i class="fa fa-sort-amount-asc"></i></a></div>
                    </div>
                </div>
                <div class="content">
                    <div class="list">


                        <!-- T a s k s -->

                        <div class="title">Today</div>
                        <ul>
                            <?php $i = 0; ?>
                            <?php foreach ($tasks as $task) : ?>
                                <?php if (explode(' ', $task->created_at)[0] == date('Y-m-d')) : ?>
                                    <li class="<?= $task->is_done ? 'checked' : '' ?>"><i data-taskId=<?= $task->id ?> class="isDone clickable <?= $task->is_done ? 'fa fa-check-square-o' : 'fa fa-square-o' ?>"></i><span><?= $task->title ?></span>
                                        <a href="?delete_task=<?= $task->id ?>&folder_id=<?= $_GET['folder_id'] ?? null ?>" class="remove" onclick="return confirm('Are you sure to delete <?= $task->title ?> ?')">X</a>
                                        <div class="info">
                                            <div class="<?= $task->is_done ? 'button green' : 'button' ?>"><?= $task->is_done ? 'Done' : 'Undone' ?></div><span>Created at <?= $task->created_at ?></span>
                                        </div>
                                    </li>
                                    <?php $i++; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <?php if ($i == 0) : ?>
                                <div>No Task Here ...</div>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <div class="list">
                        <div class="title">Before</div>
                        <ul>
                            <?php $i = 0; ?>
                            <?php foreach ($tasks as $task) : ?>
                                <?php if (explode(' ', $task->created_at)[0] < date('Y-m-d')) : ?>
                                    <li class="<?= $task->is_done ? 'checked' : '' ?>"><i data-taskId=<?= $task->id ?> class="isDone clickable <?= $task->is_done ? 'fa fa-check-square-o' : 'fa fa-square-o' ?>"></i><span><?= $task->title ?></span>
                                        <a href="<?= BASE_URL ?>?delete_task=<?= $task->id ?>&folder_id=<?= $_GET['folder_id'] ?? null ?>" class="remove" onclick="return confirm('Are you sure to delete <?= $task->title ?> ?')">x</a>
                                        <div class="info">
                                            <div class="<?= $task->is_done ? 'button green' : 'button' ?>"><?= $task->is_done ? 'Done' : 'Undone' ?></div><span>Created at <?= $task->created_at ?></span>
                                        </div>
                                    </li>
                                    <?php $i++; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <?php if ($i == 0) : ?>
                                <div>No Task Here ...</div>
                            <?php endif; ?>
                        </ul>
                    </div>

                    <!-- E n d - T a s k s -->

                </div>
            </div>
        </div>
    </div>
    <footer>
        <p>
            Back-end with PHP by Mehdi Dinarvand -
            <a target="_blank" href="https://gitlab.com/Mehtii"><b title="https://gitlab.com/Mehtii">My Gitlab</b></a>
        </p>
    </footer>


    <!-- JavaScript -->

    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="<?= site_url('assets/js/script.js') ?>"></script>
    <script>
        $('#newTaskInput').focus();
        $(document).ready(function(event) {
            var input = $('input#newFolderInput');
            $('#newFolderBtn').click(function() {
                $.ajax({
                    url: "process/ajaxHandler.php",
                    method: "post",
                    dataType: "JSON",
                    data: {
                        action: "addFolder",
                        folderName: input.val(),
                    },
                    success: function(response) {
                        if (response.name == 'addFolderError') {
                            swal('فولدر اضافه نشد!', response.description);
                        } else {
                            var id = response[0].id;
                            var name = response[0].name;
                            location.reload();
                            // $('ul.folder-list').append('<a '+'href="?folder_id='+id+'"><li><i class="fa fa-folder"</i> '+name+' </a><a href="?delete_folder='+id+'" class="remove">X</a>');
                        }
                    },
                });
            });
            $('#newTaskInput').on('keypress', function(e) {
                if (e.which == 13) {
                    // alert('You pressed enter!');
                    $.ajax({
                        url: "process/ajaxHandler.php",
                        method: 'post',
                        data: {
                            action: 'addTask',
                            taskTitle: $('#newTaskInput').val(),
                            folderId: <?= $_GET['folder_id'] ?? 0 ?>
                        },
                        success: function(response) {
                            if (response == 1) {
                                // alert(response);
                                location.reload();
                            } else {
                                swal("تسک اضافه نشد!", response);
                            }
                        }
                    });
                }
            });
            $('.isDone').click(function() {
                var tid = $(this).attr('data-taskId');
                // alert(tid);
                $.ajax({
                    url: 'process/ajaxHandler.php',
                    method: 'post',
                    data: {
                        action: 'doneSwitch',
                        taskId: tid
                    },
                    success: function(response) {
                        if (response == 1) {
                            // alert(response);
                            location.reload();
                        } else {
                            alert(response);
                        }
                    }
                });
            });
        });
    </script>
</body>

</html>