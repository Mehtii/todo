<?php defined('BASE_PATH') or die('<p style="color:red">Permission Denied!</p>'); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Todo Authentication</title>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css'>
    <link rel="stylesheet" href="<?= site_url('assets/css/auth.css') ?>">
</head>

<body>
    <script>
        if (window.history.replaceState) {
            window.history.replaceState(null, null, window.location.href);
            // https://stackoverflow.com/questions/6320113/how-to-prevent-form-resubmission-when-page-is-refreshed-f5-ctrlr?newreg=3374dfc16c844b46aca21e8db8365a90
        }
    </script>
    <!-- partial:index.partial.html -->
    <div class="container" id="container">
        <div class="form-container sign-up-container">
            <form action="<?= htmlspecialchars(site_url('auth.php?action=register')) ?>" method="post">
                <h1>Create Account</h1>
                <div class="social-container"></div>
                <input name="name" type="text" placeholder="Name" />
                <input name="email" type="email" placeholder="Email" />
                <input name="password" type="password" placeholder="Password" />
                <button>Sign Up</button>
            </form>
        </div>
        <div class="form-container sign-in-container">
            <form action="<?= htmlspecialchars(site_url('auth.php?action=login')) ?>" method="post">
                <h1>Sign in</h1>
                <div class="social-container"></div>
                <input name="username" type="text" placeholder="Email or Username" />
                <input name="password" type="password" placeholder="Password" />
                <a id="forgotPass" href="#">Forgot your password?</a>
                <button>Sign In</button>
            </form>
        </div>
        <div class="overlay-container">
            <div class="overlay">
                <div class="overlay-panel overlay-left">
                    <h1>Welcome Back!</h1>
                    <p>To keep connected with us please login with your personal info</p>
                    <button class="ghost" id="signIn">Sign In</button>
                </div>
                <div class="overlay-panel overlay-right">
                    <h1>Hello, Friend!</h1>
                    <p>Enter your personal details and start journey with us</p>
                    <button class="ghost" id="signUp">Sign Up</button>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <p>
            Back-end with PHP by Mehdi Dinarvand -
            <a target="_blank" href="https://gitlab.com/Mehtii"><b title="https://gitlab.com/Mehtii">My Gitlab</b></a>
        </p>
    </footer>

    <!-- partial -->
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script src="<?= site_url('assets/js/auth.js') ?>"></script>
    <script>
        $(document).ready(function(event) {
            $('#forgotPass').click(function() {
                alert('Soon .');
            });
        });
    </script>

</body>

</html>