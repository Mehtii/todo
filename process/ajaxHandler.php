<?php

include_once '../bootstrap/init.php';
if (!is_ajax_request()) {
    diePage('Invalid Request!');
}
if (!isset($_POST['action']) || empty($_POST['action'])) {
    diePage('Invalid Request!');
}


#-------------------------------------------------------------------------------------------
/*** Add Folder and Task ***/
// if(!isset($_POST['action']) || empty($_POST['action'])) {
//     diePage('Invalid Request!');
// }
// if ($_POST['action'] == 'addFolder' && strlen($_POST['folderName']) > 2){
//     $add_folder =  addFolder($_POST['folderName']);
//     // echo $add_folder . " row added in database.";
//     echo $add_folder;
// } else {
//     echo 'اسم فولدر باید حداقل 3 حرف باشد!';
//     die();
// }

switch ($_POST['action']) {
    case 'addFolder':
        $_POST['folderName'] = clearInput($_POST['folderName']);
        if (!isset($_POST['folderName']) || empty($_POST['folderName'])) {
            $addFolderError = ['name'=>'addFolderError','description'=>'لطفا نام فولدر را وارد کنید.'];
            echo json_encode($addFolderError);
            die();
        }
        if (strlen($_POST['folderName']) < 3) {
            $addFolderError = ['name'=>'addFolderError','description'=>'اسم فولدر باید حداقل 3 حرف باشد!'];
            echo json_encode($addFolderError);
            die();
        }
        $lastInsertId = addFolder($_POST['folderName']);
        echo json_encode(addNewFolderRow($lastInsertId));
        break;
    case 'addTask':
        $_POST['taskTitle'] = clearInput($_POST['taskTitle']);
        if ($_POST['folderId'] == 0 || !$_POST['folderId']) {
            echo 'لطفا ابتدا فولدر مورد نظر را انتخاب نمایید.';
            die();
        }
        if (!isset($_POST['taskTitle']) || empty($_POST['taskTitle'])) {
            echo 'لطفا عنوان تسک را وارد کنید.';
            die();
        }
        if (strlen($_POST['taskTitle']) < 3) {
            echo 'عنوان تسک باید حداقل 3 حرف باشد!';
            die();
        }
        echo addTask($_POST['taskTitle'], $_POST['folderId']);
        break;
    case 'doneSwitch':
        // dd($_POST);
        if (!isset($_POST['taskId']) || !is_numeric($_POST['taskId'])) {
            echo 'آیدی تسک معتبر نیست';
            die();
        }
        echo doneSwitch($_POST['taskId']);
        break;
    default:
        diePage('Invalid Request!');
}
#-------------------------------------------------------------------------------------------
